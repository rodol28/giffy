import React, { useRef, useEffect, useCallback } from "react";
import debounce from "just-debounce-it";
import { useGifs } from "hooks/useGifs";
import { useNearScreen } from "hooks/useNearScreen";
import ListOfGifs from "components/listOfGifs/ListOfGifs";
import Spinner from "components/spinner";
import "./SearchResults.css";
import { Helmet } from "react-helmet";
import SearchForm from 'components/SearchForm';

export default function SearchResults({ params }) {
  const { keyword, rating = 'g' } = params;
  const [loading, gifs, setPage] = useGifs({ keyword, rating });
  // tambien sirve para la paginacion con el boton
  const handleNextPage = () => setPage(prevPage => prevPage + 1);

  const title = gifs ? `${gifs.length} resultados de ${keyword}` : "";

  const externalRef = useRef();
  const { isNearScreen } = useNearScreen({
    externalRef: loading ? null : externalRef,
    once: false,
  });
  // eslint-disable-next-line
  const debounceHandleNextPage = useCallback(
    debounce(() => {
      handleNextPage();
    }, 200),
    [setPage]
  );

  useEffect(() => {
    if (isNearScreen) debounceHandleNextPage();
  }, [debounceHandleNextPage, isNearScreen]);

  return loading ? (
    <Spinner />
  ) : (
    <>
      <Helmet>
        <title>{title}</title>
        <meta name="description" content={title} />
      </Helmet>
      <SearchForm initialKeyword={keyword} initialRating={rating} />
      <div className="container">
        <ListOfGifs gifs={gifs} />
        {/* boton de la paginacion */}
        {/* <button onClick={handleNextPage}>Get next page</button> */}
        <div id="visor" ref={externalRef}></div>
      </div>
    </>
  );
}
