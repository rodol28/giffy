import React from "react";
import { useGifs } from "hooks/useGifs";
import ListOfGifs from "components/listOfGifs/ListOfGifs";
import "./Home.css";
import TrendingSearches from "components/trendingSearches/";
import SearchForm from "components/SearchForm";
import { Helmet } from "react-helmet";

export default function Home() {
  // eslint-disable-next-line
  const [loading, gifs] = useGifs();

  return (
    <>
      <Helmet>
        <title>Home | Giffy</title>
      </Helmet>

      <SearchForm />
      <div className="container">
        <div className="container-results">
          <h3>Last search</h3>
          <div className="results">
            <ListOfGifs gifs={gifs} />
          </div>
        </div>
        <div className="trendings">
          <TrendingSearches />
        </div>
      </div>
    </>
  );
}
