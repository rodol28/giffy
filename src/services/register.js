import { API_LOGINS } from "./settings";

export default function login({ username, email, password, confirmpass }) {
  return fetch(`${API_LOGINS}/register`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ username, email, password, confirmpass }),
  })
    .then(res => {
      if (!res.ok) throw new Error("Response is NOT ok");
      return true;
    });
}
