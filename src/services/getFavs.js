import { API_LOGINS } from "./settings";

export default function getFavs({ jwt }) {
  return fetch(`${API_LOGINS}/favs`, {
    method: "GET",
    headers: {
      "Authorization" : jwt,
      "Content-Type": "application/json",
    },
  })
    .then(res => {
      if (!res.ok) throw new Error("Response is NOT ok");
      return res.json();
    })
    .then(resJSON => {
      const { favs } = resJSON;
      return favs;
    });
}
