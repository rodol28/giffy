import { API_LOGINS } from "./settings";

export default function login({ email, password }) {
  return fetch(`${API_LOGINS}/signin`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
  })
    .then(res => {
      if (!res.ok) throw new Error("Response is NOT ok");
      return res.json();
    })
    .then(resJSON => {
      const { token } = resJSON;
      return token;
    });
}
