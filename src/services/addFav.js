import { API_LOGINS } from "./settings";

export default function addFav({ id, jwt }) {
  return fetch(`${API_LOGINS}/favs/${id}`, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ jwt }),
  })
    .then(res => {
      if (!res.ok) throw new Error("Response is NOT ok");
      return res.json();
    })
    .then(resJSON => {
      const { favs } = resJSON;
      return favs;
    });
}
