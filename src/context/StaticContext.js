import React from 'react';

const Context = React.createContext({
    name: 'rodolfo',
    isDev: true
});

export default Context;