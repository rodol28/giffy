import React from "react";
import Gif from "../gif/Gif";
import { Link } from "wouter";
import './ListOfGifs.css';

export default function ListOfGifs({ gifs }) {
  return gifs.map(({ id, title, url }) => {
    return (
      <div key={id} className="ListOfGifs">
        <Link to="/gifs/:id">
          <Gif id={id} title={title} url={url} />
        </Link>
      </div>
    );
  });
} 
