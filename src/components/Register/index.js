import React, { useState } from "react";
import register from "services/register";
import { Formik } from "formik";
import styles from "./Register.module.css";

import { useLocation } from 'wouter';


export default function Register() {
    const [_, pushLocation] = useLocation();
    const [registered, setRegistered] = useState(false);


  if (registered) {
    <h4>Congratulations! You've been successfully registrered! </h4>;
    pushLocation('login');
    return ""
  }

  return (
    <>
      <h1>Formulario de registro</h1>
      <Formik
        initialValues={{
          username: "",
          email: "",
          password: "",
          confirmpass: "",
        }}
        validate={values => {
          const errors = {};
          if (!values.username) {
            errors.username = "Required username";
          }
          if (!values.email) {
            errors.email = "Required email";
          }
          if (!values.password) {
            errors.password = "Required password";
          } else if (values.password.length < 8) {
            errors.confirmpass = "Long must be greater than 8";
          }
          if (!values.confirmpass) {
            errors.confirmpass = "Required confirm password";
          }
          return errors;
        }}
        onSubmit={(values, { setFieldError }) => {
          return register(values)
            .then(() => setRegistered(true))
            .catch(() => setFieldError("email", "This email is not valid."));
        }}
      >
        {({ handleChange, handleSubmit, isSubmitting, errors }) => (
          <form onSubmit={handleSubmit} className={styles.container_form}>
            <input
              className={styles.input_form}
              name="username"
              onChange={handleChange}
              placeholder="Username..."
            />
            {errors.username && (
              <small>
                <p className={styles.error}>{errors.username}</p>
              </small>
            )}

            <input
              className={styles.input_form}
              name="email"
              onChange={handleChange}
              placeholder="Email..."
            />
            <span>
              {errors.email ? (
                <p className={styles.error}>{errors.email}</p>
              ) : (
                ""
              )}
            </span>

            <input
              className={styles.input_form}
              name="password"
              onChange={handleChange}
              placeholder="Password..."
              type="password"
            />
            <span>
              {errors.password ? (
                <p className={styles.error}>{errors.password}</p>
              ) : (
                ""
              )}
            </span>

            <input
              className={styles.input_form}
              name="confirmpass"
              onChange={handleChange}
              placeholder="Confirm password..."
              type="password"
            />
            <span>
              {errors.confirmpass ? (
                <p className={styles.error}>{errors.confirmpass}</p>
              ) : (
                ""
              )}
            </span>

            <button
              type="submit"
              className={styles.btn_form}
              disabled={isSubmitting}
            >
              Registrase
            </button>
          </form>
        )}
      </Formik>
    </>
  );
}
