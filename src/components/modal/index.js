import React from "react";
import ReactDOM from "react-dom";
import styles from "./Modal.module.css";

function Modal({ children, onClose }) {
  return (
    <div className={styles.modal}>
      <div className={styles.model_content}>
        <button className={styles.btn_modal_close} onClick={onClose}>
          <i className="far fa-times-circle"></i>
        </button>
        {children}
      </div>
    </div>
  );
}

export default function ModalPortal({ children, onClose }) {
  return ReactDOM.createPortal(
    <Modal onClose={onClose}>{children}</Modal>,
    document.getElementById("modal-root")
  );
}
