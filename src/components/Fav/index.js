import React, { useState } from "react";
import { useUser } from "hooks/useUser";
import { useLocation } from "wouter";
import "./Fav.css";

import Modal from "components/modal";
import Login from "components/login";

export default function Fav({ id }) {
  const [_, pushLocation] = useLocation();
  const { isLogged, addFav, favs } = useUser();
  const [showModal, setShowModal] = useState(false);

  const isFaved = favs.some(favsId => favId === id);

  const handleClick = () => {
    // if (!isLogged) return pushLocation("/login");
    // isLogged && alert(id);
    if (!isLogged) return setShowModal(true);
    // addFav({id}); para agregar a favoritos
  };

  const handleCloseModal = () => {
    setShowModal(false);
  };

  const handleLogin = () => {
    setShowModal(false);
  }

  const [label, emoji] = isFaved
    ? ["Remove Gif from favorites", "❌"]
    : ["Add Gif to favorites", "💓"];

  return (
    <>
      <button onClick={handleClick} className="btn-fav">
        <span aria-label={label} role="img" className="heart">
          {emoji}
        </span>
      </button>
      {showModal && (
        <Modal onClose={handleCloseModal}>
          <Login onLogin={handleLogin}/>
        </Modal>
      )}
    </>
  );
}
