import React from "react";
import "./Gif.css";
import { Link } from "wouter";

import Fav from "components/Fav";

function Gif({ id, title, url }) {
  return (
    <div className="container">
      <div className="gif-buttons">
        <Fav id={id} />
      </div>
      <Link to={`/gif/${id}`} className="Gif">
        <div className="container-gif">
          <img src={url} alt={title} />
        </div>
      </Link>
    </div>
  );
}

export default React.memo(Gif);
