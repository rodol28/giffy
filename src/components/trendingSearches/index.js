import React, { Suspense } from "react";
import { useNearScreen } from "hooks/useNearScreen";
import Spinner from 'components/spinner'

const TrendingsSearches = React.lazy(() => import("./TrendingsSearches"));

export default function LazyTrending() {
  const { isNearScreen, fromRef } = useNearScreen({ distance: "200px" });

  return (
    <div ref={fromRef}>
      {isNearScreen ? (
        <Suspense fallback={<Spinner/>}>
          <TrendingsSearches />
        </Suspense>
      ) : <Spinner/>}
    </div>
  );
}
