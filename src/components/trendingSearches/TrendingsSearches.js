import React, { useState, useEffect } from "react";
import getTrendingTerms from "services/getTrendingTermsService";
import Category from "components/category/Category";

export default function TrendingSearches() {
  const [trends, setTrends] = useState([]);

  useEffect(() => {
    getTrendingTerms().then(res => setTrends(res));
  }, []);

  return <Category name={"Trendings"} options={trends} />;
}
