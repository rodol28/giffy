import React, { useState, useReducer } from "react";
import { useLocation } from "wouter";
import { useForm } from "./useForm";

const RATINGS = ["g", "pg", "pg-13", "r"];

function SearchForm({ initialRating = "g", initialKeyword = "" }) {

  const { keyword, times, rating, updateKeyword, updateRating } = useForm({
    initialKeyword,
    initialRating,
  });

  const [_, pushLocation] = useLocation();

  const handleSubmit = e => {
    e.preventDefault();
    pushLocation(`/search/${keyword}/${rating}`);
  };

  const handleChange = e => {
    updateKeyword(e.target.value);
  };

  const handleChangeRating = e => {
    updateRating(e.target.value);
  };

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          onChange={handleChange}
          type="text"
          value={keyword || ""}
          placeholder="Search a gif here..."
        />
        <select onChange={handleChangeRating} value={rating}>
          {RATINGS.map(rating => (
            <option key={rating}>{rating}</option>
          ))}
        </select>
      </form>
      <p>{times}</p>
    </div>
  );
}

export default React.memo(SearchForm);
