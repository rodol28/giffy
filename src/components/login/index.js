import React, { useState, useEffect } from "react";
import { useLocation } from "wouter";
import { useUser } from "hooks/useUser";
import Spinner from "components/spinner";

import styles from "./Login.module.css";

export default function Login({ onLogin }) {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const { isLoginLoading, hasLoginError, isLogged, login } = useUser();
  const [_, pushLocation] = useLocation();

  useEffect(() => {
    if (isLogged) {
      pushLocation("/");
      onLogin && onLogin();
    }
    // eslint-disable-next-line
  }, [isLogged, pushLocation]);

  const handleSubmit = e => {
    e.preventDefault();
    login({ email, password });
  };

  if (isLoginLoading) return <Spinner />;

  return (
    <div>
      {isLoginLoading && <strong>Checking credentials...</strong>}

      {!isLoginLoading && (
        <form onSubmit={handleSubmit} className={styles.login_form}>
          <input
            value={email}
            onChange={e => setEmail(e.target.value)}
            className={styles.login_input}
            type="text"
            placeholder="email..."
          />
          <input
            value={password}
            onChange={e => setPassword(e.target.value)}
            className={styles.login_input}
            type="password"
            placeholder="password..."
          />
          <button className={`${styles.login_btn}`}>Login</button>
        </form>
      )}

      {hasLoginError && <strong>Credentials are invalid.</strong>}
    </div>
  );
}