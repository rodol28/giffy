import React from "react";
import { Link, Route, Switch } from "wouter";

import Header from "components/Header";
import Detail from "./components/detail/Detail";
import Home from "./pages/home";
import SearchResults from "./pages/searchResults";
import Login from "./pages/login";
import Register from 'pages/register';

import { GifsContextProvider } from "./context/GifsContext";
import { UserContextProvider } from "context/UserContext";

import "./App.css";

function App() {
  return (
    <UserContextProvider>
      <Header />
      <div className="App">
        <section className="App-content">
          <div className="title-name">
            <Link to="/">Giffy</Link>
          </div>
          <Switch>
            <GifsContextProvider>
              <Route path="/" component={Home} exact />
              <Route
                path="/search/:keyword/:rating?"
                component={SearchResults}
                exact
              />
              <Route path="/gif/:id" component={Detail} exact />
              <Route path="/register" component={Register} exact />
              <Route path="/login" component={Login} exact />
              <Route path="/404" component={() => <h1>404 ERROR</h1>} />
            </GifsContextProvider>
          </Switch>
        </section>
      </div>
    </UserContextProvider>
  );
}

export default App;
