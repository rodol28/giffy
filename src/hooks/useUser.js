import { useState, useContext, useCallback } from "react";
import Context from "context/UserContext";
import loginService from "services/login";
import addFavService from "services/addFav";

export function useUser() {
  const { jwt, setJwt, favs, setFavs } = useContext(Context);
  const [state, setState] = useState({ loading: false, error: false });

  const login = useCallback(
    ({ email, password }) => {
      setState({ loading: true, error: false });
      loginService({ email, password })
        .then(jwt => {
          window.sessionStorage.setItem("jwt", jwt);
          setState({ loading: true, error: false });
          setJwt(jwt);
        })
        .catch(err => {
          window.sessionStorage.removeItem("jwt");
          setState({ loading: false, error: true });
          console.error(err);
        });
    },
    // eslint-disable-next-line
    [jwt, setJwt]
  );

  const logout = useCallback(() => {
    setJwt(null);
  }, [setJwt]);

  const addFav = useCallback(({ id }) => {
    addFavService({ id, jwt })
      .then(favs => setFavs(favs))
      .catch(err => console.error(err));
      // eslint-disable-next-line
  }, [jwt, setJwt]);

  return {
    favs,
    isLogged: Boolean(jwt),
    isLoginLoading: state.loading,
    hasLoginError: state.error,
    login,
    logout,
    addFav,
  };
}
