import { useState, useEffect, useContext } from "react";
import getGifs from "../services/getGifs";
import GifsContex from "../context/GifsContext";

const INITIAL_PAGE = 0;

export function useGifs({ keyword, rating } = { keyword: "" }) {
  const [loading, setLoading] = useState(false);
  const [loadingNextPage, setLoadingNextPage] = useState(false);
  const [page, setPage] = useState(INITIAL_PAGE);
  const { gifs, setGifs } = useContext(GifsContex);

  let keywordToUse = "";

  if (keyword !== "") {
    keywordToUse = keyword;
  }
  if (keyword === "") {
    if (localStorage.getItem("lastKeyword") !== "") {
      keywordToUse = localStorage.getItem("lastKeyword");
    }
  }

  if (
    (keyword === "" && localStorage.getItem("lastKeyword") === "") ||
    localStorage.getItem("lastKeyword") === null
  ) {
    keywordToUse = "random";
  }

  useEffect(() => {
    setLoading(true);
    getGifs({ limit: 5, keyword: keywordToUse, rating }).then(gifs => {
      setGifs(gifs);
      setLoading(false);
      localStorage.setItem("lastKeyword", keywordToUse);
    });
  }, [keyword, keywordToUse, setGifs, rating]);

  useEffect(() => {
    if (page === INITIAL_PAGE) return;
    
    setLoadingNextPage(true);

    getGifs({ limit: 5, keyword: keywordToUse, page, rating })
    .then(nextGifs => {
      setGifs(prevGifs => prevGifs.concat(nextGifs));
      setLoadingNextPage(false);
    });

  }, [keywordToUse, setGifs, page, rating]);

  return [loading, gifs, setPage, loadingNextPage];
}
